#pragma once
#include <array>

template<typename T, std::size_t N>
class CircularQueue {
public:
  CircularQueue();

  bool push(T const& item);
  bool push(T&& item);

  template<typename U>
  bool pop(U& item);
  bool pop();

  inline bool full() const;
  inline bool empty() const;

  inline size_t size() const;

protected:
  std::array<T, N> queue_;
  std::size_t front_;
  std::size_t back_;
  std::size_t size_;
};

template<typename T, std::size_t N>
CircularQueue<T, N>::CircularQueue()
  : front_(0), back_(0), size_(0)
{}

template<typename T, std::size_t N>
bool CircularQueue<T, N>::full() const
{
  return size_ == N;
}

template<typename T, std::size_t N>
bool CircularQueue<T, N>::empty() const
{
  return size_ == 0;
}

template<typename T, std::size_t N>
size_t CircularQueue<T, N>::size() const
{
  return size_;
}

template<typename T, std::size_t N>
bool CircularQueue<T, N>::push(T const& item)
{
  if (this->full()) return false;
  queue_[back_] = item;
  back_ = (back_ + 1) % N;
  size_ ++;
  return true;
}

template<typename T, std::size_t N>
bool CircularQueue<T, N>::push(T&& item)
{
  if (this->full()) return false;
  queue_[back_] = std::move(item);
  back_ = (back_ + 1) % N;
  size_ ++;
  return true;
}

template<typename T, std::size_t N>
template<typename U>
bool CircularQueue<T, N>::pop(U& item)
{
  if (this->empty()) return false;
  item = std::move(queue_[front_]);
  front_ = (front_ + 1) % N;
  size_ --;
  return true;
}

template<typename T, std::size_t N>
bool CircularQueue<T, N>::pop()
{
  if (this->empty()) return false;
  front_ = (front_ + 1) % N;
  size_ --;
  return true;
}
