#pragma once
#include <mutex>
#include "circularqueue.h"

template<typename T, std::size_t N>
class BlockingCircularQueue : public CircularQueue<T, N> {
public:
  BlockingCircularQueue();

  bool push(T const& item);
  bool push(T&& item);

  template<typename U>
  bool pop(U& item);
  bool pop();

protected:
  std::mutex mutex_;
};

template<typename T, std::size_t N>
BlockingCircularQueue<T, N>::BlockingCircularQueue()
  : CircularQueue<T, N>::CircularQueue()
{}

template<typename T, std::size_t N>
bool BlockingCircularQueue<T, N>::push(T const& item)
{
  std::lock_guard<std::mutex> guard(mutex_);
  return CircularQueue<T, N>::push(item);
}

template<typename T, std::size_t N>
bool BlockingCircularQueue<T, N>::push(T&& item)
{
  std::lock_guard<std::mutex> guard(mutex_);
  return CircularQueue<T, N>::push(std::move(item));
}

template<typename T, std::size_t N>
template<typename U>
bool BlockingCircularQueue<T, N>::pop(U& item)
{
  std::lock_guard<std::mutex> guard(mutex_);
  return CircularQueue<T, N>::pop(item);
}

template<typename T, std::size_t N>
bool BlockingCircularQueue<T, N>::pop()
{
  std::lock_guard<std::mutex> guard(mutex_);
  return CircularQueue<T, N>::pop();
}


enum class DropStrategy {
  DROP_FRONT,
  DROP_BACK
};

template<typename T, std::size_t N>
class BlockingDropCircularQueue : public BlockingCircularQueue<T, N> {
public:
  BlockingDropCircularQueue(DropStrategy strategy=DropStrategy::DROP_FRONT);

  bool push(T const& item);
  bool push(T&& item);

protected:
  DropStrategy strategy_;
};

template<typename T, std::size_t N>
BlockingDropCircularQueue<T, N>::BlockingDropCircularQueue(DropStrategy strategy)
  : BlockingCircularQueue<T, N>::BlockingCircularQueue(),
    strategy_(strategy)
{}

template<typename T, std::size_t N>
bool BlockingDropCircularQueue<T, N>::push(T const& item)
{
  while (this->full()) {
    if (strategy_ == DropStrategy::DROP_BACK) return true;
    if (!this->pop()) return false;
  }
  return BlockingCircularQueue<T, N>::push(item);
}

template<typename T, std::size_t N>
bool BlockingDropCircularQueue<T, N>::push(T&& item)
{
  while (this->full()) {
    if (strategy_ == DropStrategy::DROP_BACK) return true;
    if (!this->pop()) return false;
  }
  return BlockingCircularQueue<T, N>::push(std::move(item));
}
