#include "blockingcircularqueue.h"

int main () {
  BlockingDropCircularQueue<int, 10> Q;

  for (int i = 0; i < 20; i ++) {
    bool ret = Q.push(i);
    printf("push %d: %s\n", i, ret ? "SUCCESS" : "FAIL");
  }
  printf("size: %lu\n", Q.size());

  while (!Q.empty()) {
    int item;
    Q.pop(item);
    printf("pop %d\n", item);
  }
  printf("size: %lu\n", Q.size());

  return 0;
}
