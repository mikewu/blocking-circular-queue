#include "circularqueue.h"

int main () {
  CircularQueue<int, 10> Q;

  for (int i = 0; i < 20; i ++) {
    bool ret = Q.push(i);
    printf("push %d: %s\n", i, ret ? "SUCCESS" : "FAIL");
  }
  printf("size: %lu\n", Q.size());

  printf("===========\n");

  for (int i = 0; i < 5; i ++) {
    int item = 0;
    bool ret = Q.pop(item);
    if (ret)
      printf("pop %d\n", item);
    else
      printf("pop FAIL\n");
  }
  printf("size: %lu\n", Q.size());

  printf("===========\n");

  for (int i = 0; i < 20; i ++) {
    bool ret = Q.push(i+100);
    printf("push %d: %s\n", i, ret ? "SUCCESS" : "FAIL");
  }
  printf("size: %lu\n", Q.size());

  printf("===========\n");

  printf("size: %lu\n", Q.size());
  while (!Q.empty()) {
    int item;
    Q.pop(item);
    printf("pop %d\n", item);
  }
  printf("size: %lu\n", Q.size());

  return 0;
}
